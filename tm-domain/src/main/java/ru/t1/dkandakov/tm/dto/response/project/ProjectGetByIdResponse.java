package ru.t1.dkandakov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.model.Project;


@NoArgsConstructor
public final class ProjectGetByIdResponse extends AbstractProjectResponse {

    public ProjectGetByIdResponse(@Nullable Project project) {
        super(project);
    }

}


