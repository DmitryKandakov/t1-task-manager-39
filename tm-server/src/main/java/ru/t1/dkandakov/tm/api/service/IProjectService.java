package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.enumerated.ProjectSort;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @Nullable
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @Nullable
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @NotNull
    Project removeOneById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    void removeAll(@NotNull String userId);

    void removeAll();

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    int getSize(@Nullable String userId);

    @Nullable
    Project removeOneById(@Nullable String id);

    @Nullable
    Project removeOneByIndex(@Nullable Integer index);

    @NotNull
    Project removeOne(@Nullable Project project);

    @Nullable
    Project removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    Project updateByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    );

    @Nullable
    Project findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    Project findOneById(@Nullable String id);

    @NotNull
    Collection<Project> add(@NotNull Collection<Project> models);

    @NotNull
    Collection<Project> set(@NotNull Collection<Project> models);

    @NotNull
    String getSortType(@Nullable ProjectSort sort);

    int getSize();

    @Nullable
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}

