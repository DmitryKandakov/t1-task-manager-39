package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.model.Session;

import java.util.List;

public interface ISessionService {

    @NotNull
    Session add(@Nullable String userId, @Nullable Session model);

    @Nullable
    List<Session> findAll();

    @Nullable
    Session findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Session remove(@Nullable Session model);

    @NotNull
    Session removeOneById(@Nullable String userId, @Nullable String id);

}